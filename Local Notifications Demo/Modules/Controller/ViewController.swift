//
//  ViewController.swift
//  Local Notifications Demo
//
//  Created by Brandon Suarez on 6/24/21.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {
    
    @IBOutlet private weak var userNameTextField: UITextField! {
        didSet {
            self.userNameTextField.layer.borderColor = CGColor(red: 0, green: 0, blue: 0, alpha: 1)
            self.userNameTextField.layer.borderWidth = CGFloat(1.0)
//            self.userNameTextField.layer.backgroundColor = CGColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        }
    }
    @IBOutlet private weak var passwordTextField: UITextField! {
        didSet {
            self.passwordTextField.layer.borderColor = CGColor(red: 0, green: 0, blue: 0, alpha: 1)
            self.passwordTextField.layer.borderWidth = CGFloat(1.0)
//            self.passwordTextField.layer.backgroundColor = CGColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        }
    }
    @IBOutlet private weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeRound(with: 2.0)
        
        // Step 1: Ask for permission
        let manager = UNUserNotificationCenter.current()
        manager.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
        }
        // Step 2: Create the notification's content
        let content = UNMutableNotificationContent()
        content.title = "Hey This is a custom notification"
        content.body = "This is the Notification's Content"
        
        // Step 3: Crete the notification's trigger
        let date = Date().addingTimeInterval(10)
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        // Step 4: Create the request
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
        
        // Step 5: Register the request
        manager.add(request) { error in
        }
    }
    
    @IBAction private func openLocalNotification(_ sender: Any) {
//        let manager = UNUserNotificationCenter.current()
//        manager.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
//        }
//        let content = UNMutableNotificationContent()
//        content.title = "User Accepted"
//        content.body = "A user has successfully logged in"
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
//        let uuidString = UUID().uuidString
//        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)
//        manager.add(request)
    }
    
    func makeRound(with aRadius: CGFloat) {
        self.userNameTextField.layer.masksToBounds = true
        self.passwordTextField.layer.masksToBounds = true
        self.loginButton.layer.masksToBounds = true
        
        self.userNameTextField.layer.cornerRadius = self.userNameTextField.frame.height / aRadius
        self.passwordTextField.layer.cornerRadius = self.passwordTextField.frame.height / aRadius
        self.loginButton.layer.cornerRadius = self.loginButton.frame.height / aRadius
    }
}

